# treasure_hunting.TreasureHunting

- Imagine you are a pirate somewhere on the secret Island in the ocean. You're eager to find the treasure, and you know its location thanks to the old map. All you need is to follow this map.



- In this task you need to implement the method GoToPoint(Pirate pirate, int toX, int toY) of the interface TreasureHunting. In this method you receive the pirate and the coordinates of the treasure. You are also given the Pirate class, which contains the following methods:


           - getX() returns the X coordinate of current location of the pirate
           - getY() returns the Y coordinate of current location of the pirate
           - turnLeft() change the direction of pirate to 90 degrees left
           - turnRight() change the direction of pirate to 90 degrees right
           - getDirection() returns the current direction of the pirate
           - stepForward() moves pirate to one step according to the direction

- Note that Direction.UP corresponds with North (↑), Direction.DOWN corresponds with South (↓), Direction.LEFT corresponds with West (←), Direction.RIGHT corresponds with East (→).


- Wish you a good luck!!! Hope you'll succeed in hunting. 