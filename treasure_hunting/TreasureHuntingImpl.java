package treasure_hunting;

import treasure_hunting.Direction;
import treasure_hunting.Pirate;
import treasure_hunting.TreasureHunting;

public class TreasureHuntingImpl implements TreasureHunting {

    @Override
    public void GoToPoint(Pirate pirate, int toX, int toY) {
        int fromX = pirate.getX();
        int fromY = pirate.getY();
        if (toX >= fromX){
            while(pirate.getDirection() != Direction.RIGHT){
                pirate.turnRight();
            }
            while(pirate.getX() != toX){
                pirate.stepForward();
            }
            if (toY >= fromY){
                pirate.turnLeft();
                while(pirate.getY() != toY){
                    pirate.stepForward();
                }
            }else{
                pirate.turnRight();
                while(pirate.getY() != toY){
                    pirate.stepForward();
                }
            }
        }else{
            while(pirate.getDirection() != Direction.LEFT){
                pirate.turnLeft();
            }
            while(pirate.getX() != toX){
                pirate.stepForward();
            }
            if (toY >= fromY){
                pirate.turnRight();
                while(pirate.getY() != toY){
                    pirate.stepForward();
                }
            }else{
                pirate.turnLeft();
                while(pirate.getY() != toY){
                    pirate.stepForward();
                }
            }
        }
    }
}
