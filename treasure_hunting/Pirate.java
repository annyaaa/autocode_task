package treasure_hunting;

import treasure_hunting.Direction;

import java.util.Random;

public class Pirate {

    Random random = new Random();
    private int position_x;
    private int position_y;
    private Direction direction;

    public Pirate(){
        this.position_x = random.nextInt(1000000);
        this.position_y = random.nextInt(1000000);
        this.direction = Direction.UP;
    }

    public int getX(){
        return this.position_x;
    }

    public int getY(){
        return this.position_y;
    }
    public Direction getDirection() {
        return this.direction;
        // текущее направление взгляда
    }
    
    public void turnLeft() {
        if (this.direction == Direction.UP){
            this.direction = Direction.LEFT;
        }else if(this.direction == Direction.LEFT){
            this.direction = Direction.DOWN;
        }else if(this.direction == Direction.DOWN){
            this.direction = Direction.RIGHT;
        }else{
            this.direction = Direction.UP;
        }
        // повернуться на 90 градусов против часовой стрелки
    }

    public void turnRight() {
        if (this.direction == Direction.UP){
            this.direction = Direction.RIGHT;
        }else if(this.direction == Direction.RIGHT){
            this.direction = Direction.DOWN;
        }else if(this.direction == Direction.DOWN){
            this.direction = Direction.LEFT;
        }else{
            this.direction = Direction.UP;
        }
        // повернуться на 90 градусов по часовой стрелке
    }

    public void stepForward() {
        if (this.direction == Direction.UP){
            this.position_y = this.position_y + 1;
        }else if(this.direction == Direction.RIGHT){
            this.position_x = this.position_x + 1;
        }else if(this.direction == Direction.DOWN){
            this.position_y = this.position_y - 1;
        }else{
            this.position_x = this.position_x - 1;
        }
        // шаг в направлении взгляда
        // за один шаг пират изменяет одну свою координату на единицу
    }

}
