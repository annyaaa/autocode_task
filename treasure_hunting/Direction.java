package treasure_hunting;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
