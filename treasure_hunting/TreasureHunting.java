package treasure_hunting;

import treasure_hunting.Pirate;

public interface TreasureHunting {
    public void GoToPoint(Pirate pirate, int toX, int toY);
}
