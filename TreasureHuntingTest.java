import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import treasure_hunting.Pirate;
import treasure_hunting.TreasureHuntingImpl;

import java.util.Random;

class TreasureHuntingTest {
    TreasureHuntingImpl treasureHunting = new TreasureHuntingImpl();

    @Test
    void testTreasureHuntingNorthWest() {
        Random random = new Random();
        Pirate pirate = new Pirate();
        int X = pirate.getX() - random.nextInt(1000000);
        int Y = pirate.getY() + random.nextInt(1000000);
        treasureHunting.GoToPoint(pirate, X, Y);
        assertEquals(pirate.getX(), X);
        assertEquals(pirate.getY(), Y);
    }

    @Test
    void testTreasureHuntingNorthEast() {
        Random random = new Random();
        Pirate pirate = new Pirate();
        int X = pirate.getX() + random.nextInt(1000000);
        int Y = pirate.getY() + random.nextInt(1000000);
        treasureHunting.GoToPoint(pirate, X, Y);
        assertEquals(pirate.getX(), X);
        assertEquals(pirate.getY(), Y);
    }

    @Test
    void testTreasureHuntingSouthWest() {
        Random random = new Random();
        Pirate pirate = new Pirate();
        int X = pirate.getX() - random.nextInt(1000000);
        int Y = pirate.getY() - random.nextInt(1000000);
        treasureHunting.GoToPoint(pirate, X, Y);
        assertEquals(pirate.getX(), X);
        assertEquals(pirate.getY(), Y);
    }

    @Test
    void testTreasureHuntingSouthEast() {
        Random random = new Random();
        Pirate pirate = new Pirate();
        int X = pirate.getX() + random.nextInt(1000000);
        int Y = pirate.getY() - random.nextInt(1000000);
        treasureHunting.GoToPoint(pirate, X, Y);
        assertEquals(pirate.getX(), X);
        assertEquals(pirate.getY(), Y);
    }

    @Test
    void testTreasureHuntingSouth() {
        Random random = new Random();
        Pirate pirate = new Pirate();
        int X = pirate.getX();
        int Y = pirate.getY() - random.nextInt(1000000);
        treasureHunting.GoToPoint(pirate, X, Y);
        assertEquals(pirate.getX(), X);
        assertEquals(pirate.getY(), Y);
    }

    @Test
    void testTreasureHuntingEast() {
        Random random = new Random();
        Pirate pirate = new Pirate();
        int X = pirate.getX() + random.nextInt(1000000);
        int Y = pirate.getY();
        treasureHunting.GoToPoint(pirate, X, Y);
        assertEquals(pirate.getX(), X);
        assertEquals(pirate.getY(), Y);
    }

    @Test
    void testTreasureHuntingNorth() {
        Random random = new Random();
        Pirate pirate = new Pirate();
        int X = pirate.getX();
        int Y = pirate.getY() + random.nextInt(1000000);
        treasureHunting.GoToPoint(pirate, X, Y);
        assertEquals(pirate.getX(), X);
        assertEquals(pirate.getY(), Y);
    }

    @Test
    void testTreasureHuntingWest() {
        Random random = new Random();
        Pirate pirate = new Pirate();
        int X = pirate.getX() - random.nextInt(1000000);
        int Y = pirate.getY();
        treasureHunting.GoToPoint(pirate, X, Y);
        assertEquals(pirate.getX(), X);
        assertEquals(pirate.getY(), Y);
    }
}



